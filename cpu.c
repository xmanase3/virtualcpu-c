#include "cpu.h"
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <assert.h>

static int8_t increaseMemory(uint8_t **memory, size_t *pages_allocated)
{
    uint8_t *alloc_address = NULL;
    if (!(alloc_address = realloc(*memory, (*pages_allocated + 1) * 4096))) {
        free(*memory);
        return 0;
    }
    *memory = alloc_address;
    memset((*memory + *pages_allocated * 4096), 0, 4096);
    *pages_allocated += 1;
    return 1;
}

int32_t *cpuCreateMemory(FILE *program, size_t stackCapacity, int32_t **stackBottom)
{
    assert(program != NULL);
    assert(stackBottom != NULL);
    *stackBottom = NULL;
    uint8_t *memory = NULL;
    size_t bytes_loaded = 0;
    size_t pages_allocated = 0;
    int16_t input = 0;
    while ((input = fgetc(program)) != EOF) {
        // memory allocation
        if (bytes_loaded % 4096 == 0) {
            if (!increaseMemory(&memory, &pages_allocated)) {
                return NULL;
            }
        }
        memory[bytes_loaded] = input;
        bytes_loaded++;
    }

    // file is empty and stack is zero
    if (bytes_loaded == 0 && stackCapacity == 0) {
        return NULL;
    }
    // invalid count of bytes in file
    if (bytes_loaded % 4 != 0) {
        free(memory);
        return NULL;
    }
    // stack reallocation
    while (bytes_loaded + stackCapacity * 4 > pages_allocated * 4096) {
        if (!increaseMemory(&memory, &pages_allocated)) {
            return NULL;
        }
    }
    (*stackBottom) = (int32_t *) (memory + pages_allocated * 4096 - 4);
    return (int32_t *) memory;
}

void cpuCreate(struct cpu *cpu, int32_t *memory, int32_t *stackBottom, size_t stackCapacity)
{
    assert(cpu != NULL);
    assert(memory != NULL);
    assert(stackBottom != NULL);
    memset(cpu, 0, sizeof(struct cpu));
    cpu->memory = memory;
    cpu->stackBottom = stackBottom;
    cpu->stackLimit = stackBottom - stackCapacity;
}

void cpuDestroy(struct cpu *cpu)
{
    assert(cpu != NULL);
    free(cpu->memory);
    memset(cpu, 0, sizeof(struct cpu));
}

static int32_t *checkValidStackAddress(struct cpu *cpu, int32_t parameter)
{
    int32_t index = (cpu->D + parameter);
    int32_t *address = (cpu->stackBottom - cpu->stackSize + 1) + index;
    if (address > cpu->stackBottom || address <= cpu->stackLimit || index < 0) {
        cpu->status = cpuInvalidStackOperation;
        return NULL;
    }
    return address;
}

static int32_t checkIndex(struct cpu *cpu, int32_t index)
{
    if (index < 0 || cpu->memory + index > cpu->stackLimit) {
        cpu->status = cpuInvalidAddress;
        return 0;
    }
    return 1;
}

static enum instructions loadInstruction(struct cpu *cpu, int32_t *first_parameter, int32_t *second_parameter)
{
    int8_t instruction_value;
    if (cpu->status != cpuOK || !checkIndex(cpu, cpu->instructionPointer)) {
        return NONE;
    }
    instruction_value = cpu->memory[cpu->instructionPointer];
    // check if instruction is in range
    if (instruction_value < 0 || instruction_value > 25) {
        return NONE;
    }
    return instruction_value;
}

static int8_t loadFromMemory(struct cpu *cpu, int32_t *parameter, uint8_t parameter_number)
{
    if (!checkIndex(cpu, cpu->instructionPointer + parameter_number)) {
        return 0;
    }
    *parameter = cpu->memory[cpu->instructionPointer + parameter_number];
    return 1;
}

static int8_t loadParameters(struct cpu *cpu, int32_t *first_parameter,
                             int32_t *second_parameter, enum instructions instruction)
{
    // all instructions except nop
    if (instruction > 1 && instruction < 25) {
        if (!loadFromMemory(cpu, first_parameter, 1)) {
            return 0;
        }
    }
    // instructions: movr, load, store, swap, cmp
    if ((instruction > 8 && instruction < 12) || instruction == 16 || instruction == 19) {
        if (!loadFromMemory(cpu, second_parameter, 2)) {
            return 0;
        }
    }
    return 1;
}

static int32_t *enumRegister(struct cpu *cpu, int32_t num, enum instructions instruction)
{
    switch (num) {
    case 0:
        return &cpu->A;
    case 1:
        return &cpu->B;
    case 2:
        return &cpu->C;
    case 3:
        return &cpu->D;
    case 4:
        // instructions without access to this register
        if (instruction == inc || instruction == dec || instruction == movr ||
                instruction == load || instruction == in || instruction == get ||
                instruction == swap || instruction == pop) {
            cpu->status = cpuIllegalOperand;
            return NULL;
        }
        return &cpu->result;
    default:
        cpu->status = cpuIllegalOperand;
        return NULL;
    }
}

static int8_t execute(struct cpu *cpu, enum instructions curr_instruction,
                      int32_t first_parameter, int32_t second_parameter)
{
    int32_t next_instruction_shift = 0;
    int32_t *reg_address = NULL;
    int32_t *mem_address = NULL;
    int16_t c = 0;
    int32_t input = 0;

    if (cpu->status == cpuOK && curr_instruction > 1 && curr_instruction < 19 && curr_instruction != 8) {
        reg_address = enumRegister(cpu, first_parameter, curr_instruction);
    }

    if (cpu->status != cpuOK) {
        return 0;
    }

    switch (curr_instruction) {
    case nop:
        cpu->instructionPointer++;
        return 0;

    case halt:
        cpu->status = cpuHalted;
        next_instruction_shift = 1;
        break;

    case add:
        cpu->A += *reg_address;
        next_instruction_shift = 2;
        break;

    case sub:
        cpu->A -= *reg_address;
        next_instruction_shift = 2;
        break;

    case mul:
        cpu->A *= *reg_address;
        next_instruction_shift = 2;
        break;

    case divR:
        if (*(reg_address) == 0) {
            cpu->status = cpuDivByZero;
            return 0;
        }
        cpu->A /= *reg_address;
        next_instruction_shift = 2;
        break;

    case inc:
        *(reg_address) += 1;
        next_instruction_shift = 2;
        break;

    case dec:
        *(reg_address) -= 1;
        next_instruction_shift = 2;
        break;

    case loop:
        if (cpu->C != 0) {
            cpu->instructionPointer = first_parameter;
            return 1;
        }
        next_instruction_shift = 2;
        break;

    case movr:
        *reg_address = second_parameter;
        next_instruction_shift = 3;
        break;

    case load:
        if ((mem_address = checkValidStackAddress(cpu, second_parameter)) == NULL) {
            return 0;
        }
        *reg_address = *mem_address;
        next_instruction_shift = 3;
        break;

    case store:
        if ((mem_address = checkValidStackAddress(cpu, second_parameter)) == NULL) {
            return 0;
        }
        *mem_address = *reg_address;
        next_instruction_shift = 3;
        break;

    case in:;
        int8_t convert_status;
        if ((convert_status = scanf("%d", &input)) == 0) {
            cpu->status = cpuIOError;
            return 0;
        }
        if (convert_status == EOF) {
            cpu->C = 0;
            *reg_address = -1;
            break;
        }
        *reg_address = input;
        next_instruction_shift = 2;
        break;

    case get:
        if ((c = getchar()) == EOF) {
            cpu->C = 0;
            *reg_address = -1;
            break;
        }
        *reg_address = (int32_t) c;
        next_instruction_shift = 2;
        break;

    case out:
        printf("%d", *reg_address);
        next_instruction_shift = 2;
        break;

    case put:
        if (*reg_address < 0 || *reg_address >= 256) {
            cpu->status = cpuIllegalOperand;
            return 0;
        }
        putchar(*reg_address);
        next_instruction_shift = 2;
        break;

    case swap:;
        int32_t *reg_address2 = enumRegister(cpu, second_parameter, curr_instruction);
        int32_t temp = *reg_address;
        if (cpu->status != cpuOK) {
            return 0;
        }
        *reg_address = *reg_address2;
        *reg_address2 = temp;
        next_instruction_shift = 3;
        break;

    case push:
        if (cpu->stackBottom - cpu->stackSize <= cpu->stackLimit) {
            cpu->status = cpuInvalidStackOperation;
            return 0;
        }
        *(cpu->stackBottom - cpu->stackSize) = *reg_address;
        cpu->stackSize++;
        next_instruction_shift = 2;
        break;

    case pop:
        if (cpu->stackSize == 0) {
            cpu->status = cpuInvalidStackOperation;
            return 0;
        }
        cpu->stackSize--;
        *reg_address = *(cpu->stackBottom - cpu->stackSize);
        next_instruction_shift = 2;
        break;

    case cmp:
        cpu->result = *enumRegister(cpu, first_parameter, curr_instruction) -
                *enumRegister(cpu, second_parameter, curr_instruction);
        if (cpu->status != cpuOK) {
            return 0;
        }
        next_instruction_shift = 3;
        break;

    case jmp:
        cpu->instructionPointer = first_parameter;
        break;

    case jz:
        if (cpu->result == 0) {
            cpu->instructionPointer = first_parameter;
        } else {
            next_instruction_shift = 2;
        }
        break;

    case jnz:
        if (cpu->result != 0) {
            cpu->instructionPointer = first_parameter;
        } else {
            next_instruction_shift = 2;
        }
        break;

    case jgt:
        if (cpu->result > 0) {
            cpu->instructionPointer = first_parameter;
        } else {
            next_instruction_shift = 2;
        }
        break;

    case call:
        if (cpu->stackBottom - cpu->stackSize <= cpu->stackLimit) {
            cpu->status = cpuInvalidStackOperation;
            return 0;
        }
        *(cpu->stackBottom - cpu->stackSize) = cpu->instructionPointer + 2;
        cpu->instructionPointer = first_parameter;
        cpu->stackSize++;
        break;

    case ret:
        if (cpu->stackSize == 0) {
            cpu->status = cpuInvalidStackOperation;
            return 0;
        }
        cpu->stackSize--;
        cpu->instructionPointer = *(cpu->stackBottom - cpu->stackSize);
        break;

    default:
        cpu->status = cpuIllegalInstruction;
        return 0;
    }

    if (curr_instruction > 1 && curr_instruction < 8) {
        if (curr_instruction == 6 || curr_instruction == 7) {
            cpu->result = *reg_address;
        } else {
            cpu->result = cpu->A;
        }
    }
    cpu->instructionPointer += next_instruction_shift;
    return 1;
}

int cpuStep(struct cpu *cpu)
{
    assert(cpu != NULL);
    enum instructions curr_instruction;
    int32_t first_parameter;
    int32_t second_parameter;

    //load data
    curr_instruction = loadInstruction(cpu, &first_parameter, &second_parameter);
    loadParameters(cpu, &first_parameter, &second_parameter, curr_instruction);

    if (cpu->status != cpuOK) {
        return 0;
    }
    return execute(cpu, curr_instruction, first_parameter, second_parameter);
}

uint8_t cpuRun(struct cpu *cpu)
{
    assert(cpu != NULL);
    if (cpu->status != cpuOK) {
        return 0;
    }
    // run
    while (cpuStep(cpu)) {
    }
    // return 1 if ended successfully, 0 otherwise
    if (cpu->status == cpuHalted) {
        return 1;
    }
    return 0;
}

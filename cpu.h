/* Represents a 32-bit processor */
#include <stdint.h>
#include <stdio.h>

#ifndef CPU_H
#define CPU_H

enum cpuStatus
{
    cpuOK,
    cpuHalted,
    cpuIllegalInstruction,
    cpuIllegalOperand,
    cpuInvalidAddress,
    cpuInvalidStackOperation,
    cpuDivByZero,
    cpuIOError
};

struct cpu
{
    int32_t A;
    int32_t B;
    int32_t C;
    int32_t D;
    int32_t instructionPointer;
    int32_t *stackBottom;
    int32_t *stackLimit;
    int32_t *memory;
    int32_t stackSize;
    enum cpuStatus status;
    int32_t result;
};

enum instructions
{
    NONE = -1,
    nop,   // 0
    halt,  // 1
    add,   // 2
    sub,   // 3
    mul,   // 4
    divR,  // 5
    inc,   // 6
    dec,   // 7
    loop,  // 8
    movr,  // 9
    load,  // 10
    store, // 11
    in,    // 12
    get,   // 13
    out,   // 14
    put,   // 15
    swap,  // 16
    push,  // 17
    pop,   // 18
    cmp,   // 19
    jmp,   // 20
    jz,    // 21
    jnz,   // 22
    jgt,   // 23
    call,  // 24
    ret    // 25
};

int32_t *cpuCreateMemory(FILE *program, size_t stackCapacity, int32_t **stackBottom);
void cpuCreate(struct cpu *cpu, int32_t *memory, int32_t *stackBottom, size_t stackCapacity);
void cpuDestroy(struct cpu *cpu);
int cpuStep(struct cpu *cpu);
uint8_t cpuRun(struct cpu *cpu);
#endif
